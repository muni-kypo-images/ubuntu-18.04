# Changelog

## [vbox-0.3.0] - 2022-04-01
### Added
- Ansible

## [qemu-0.3.0], [vbox-0.2.0] - 2022-03-31
### Added
- Git client, CA certificates

## [qemu-0.2.0] - 2022-01-29
### Changed
- Ubuntu version from 18.04.5 to 18.04.6
### Fixed
- Hostname is added on boot to /etc/hosts

## [qemu-0.1.0], [vbox-0.1.0] - 2021-06-22
### Added
- First version

[qemu-0.1.0]: https://gitlab.ics.muni.cz/muni-kypo-images/ubuntu-18.04/-/tree/qemu-0.1.0
[vbox-0.1.0]: https://gitlab.ics.muni.cz/muni-kypo-images/ubuntu-18.04/-/tree/vbox-0.1.0
[qemu-0.2.0]: https://gitlab.ics.muni.cz/muni-kypo-images/ubuntu-18.04/-/tree/qemu-0.2.0
[vbox-0.2.0]: https://gitlab.ics.muni.cz/muni-kypo-images/ubuntu-18.04/-/tree/vbox-0.2.0
[qemu-0.3.0]: https://gitlab.ics.muni.cz/muni-kypo-images/ubuntu-18.04/-/tree/qemu-0.3.0
[vbox-0.3.0]: https://gitlab.ics.muni.cz/muni-kypo-images/ubuntu-18.04/-/tree/vbox-0.3.0
